# Data Analysis for Adipose insulin response in human Project
#
# This script will generate Figure 3C and 3D
# 
# Author: Enrichetta Mileti
########################## Load Libraries ########################
library(tibble)
library(ggpubr)
library(tidyr)
library(dplyr)
library(data.table)

########################## Load GSEA results #################################
summary<-read.table("/Data/Additional Data/GSEA/Summary.txt",header=T,sep="\t")

GSEA_NO<-read.table("/Data/Additional Data/GSEA/GSEA_wiki_NO_Exp_score_withNES.txt",header=T,sep="\t")
GSEA_OB<-read.table("/Data/Additional Data/GSEA/GSEA_wiki_OB_Exp_score_withNES.txt",header=T,sep="\t")
GSEA_POB<-read.table("/Data/Additional Data/GSEA/GSEA_wiki_POB_Exp_score_withNES.txt",header=T,sep="\t")

# GSEA plot results (clusters)

cluster<-read.table("/Data/Additional Data/GSEA/Summary_GSEA_Clusters.txt",header=T,sep="\t")
cluster<-unique(data.frame(Description=cluster$Description,class=cluster$class,curated.name=cluster$curated.name))


## Formatting GSEA results table
GSEA_NO<-GSEA_NO %>%
  separate(Description, c("Description1", "DB","DB_IDs"), "\\%")
GSEA_NO$Set<-"NO"
GSEA_OB<-GSEA_OB %>%
  separate(Description, c("Description1", "DB","DB_IDs"), "\\%")
GSEA_OB$Set<-"OB"
GSEA_POB<-GSEA_POB %>%
  separate(Description, c("Description1", "DB","DB_IDs"), "\\%")
GSEA_POB$Set<-"POB"

GSEA_NO.1<-merge(GSEA_NO,summary[,c("Description","sample_group","class")],by.x=c("Description1","Set"),by.y=c("Description","sample_group"))
GSEA_OB.1<-merge(GSEA_OB,summary[,c("Description","sample_group","class")],by.x=c("Description1","Set"),by.y=c("Description","sample_group"))
GSEA_POB.1<-merge(GSEA_POB,summary[,c("Description","sample_group","class")],by.x=c("Description1","Set"),by.y=c("Description","sample_group"))

df<-rbind.data.frame(GSEA_NO.1,GSEA_OB.1,GSEA_POB.1)
df$number_of_genes<-count.fields(textConnection(as.character(df$genes)), sep = "/")

df.1<-df %>% group_by(class,Set)%>%tally()
df.1<-data.frame(arrange(df.1,(class)))
df.1$class<-(as.factor(df.1$class))
df.1$class <- factor(df.1$class, levels=c("POB_spec","OB_spec","NO_lost","NO_spec","OB_lost","POB_lost","common"))

############## Figure 5A ########################

Figure_3C<-ggbarplot(df.1, x = "Set", y = "n",fill="class",label=T,ylab="Number of significant",
               palette =c("#7fbc41", "#CC0000","#4C4CFF","#dd3497","#FFD64D",'white',"#FF8C00")
)+border()+rremove('xlab')+rremove("legend")

Figure_3C

create_collapesed_df<-function(df,name){
  df<-df[df$class==name,]
  df<-merge(df,cluster[,c("Description","curated.name")],by.x="Description1",by.y="Description")
  df.2<-data.frame(df%>% 
                     mutate(genes = strsplit(as.character(genes), "/")) %>% 
                     unnest(genes))
  
  df.2<-df.2[!df.2$curated.name=="Other",]
  average_NES<-data.frame(df.2%>% 
                            group_by(curated.name,Set) %>%
                            summarise_at(vars(NES), funs(mean(., na.rm=TRUE))))
  
  df.3<-unique(cbind.data.frame(curated.name=df.2$curated.name,genes=df.2$genes,Set=df.2$Set))
  df.3 <- data.frame(df.3 %>%
                       group_by(curated.name,Set) %>% 
                       summarise(genes=paste(genes,collapse="/")))
  df.3$genes<-as.factor(df.3$genes)
  df.3$number_of_genes<-count.fields(textConnection(as.character(df.3$genes)), sep = "/")
  df.4<-merge(df.3,average_NES,by=c("curated.name","Set"))
  return(df.4)
}


plot_GSEA_collapsed<-function(df,palette){
  ggscatter(df,x="NES",y="curated.name",size = "number_of_genes",color="Set"
            ,palette=palette,alpha=0.5,xlab="average NES",ylab="Enriched WIKI Pathways")+font("y.text",size=7)+
    theme(strip.text.y = element_text(angle = 0))+ scale_size_continuous(limits=c(1,110),breaks=c(10,20,40,60,80,100))+
    border()+ geom_vline(xintercept = 0,color = "black")+ grids(axis="y")+
    xlim(-3,3)+rremove("ylab")+
    font("xy.text", size = 12)#+rremove("y.text")
}

##### Figure 3D (Common) ######
# Common GSEA results

df.common<-create_collapesed_df(df,"common")
df.common$curated.name <- reorder(df.common$curated.name, df.common$NES)

common_palette=c("#de77ae", "#5590CC","#7fbc41")
Figure_3D_Common<-plot_GSEA_collapsed(df.common,common_palette)

##### Figure 3D (OB-attenuated) ######
# Obesity attenuated GSEA results

df.ob.lost<-create_collapesed_df(df,"OB_lost")
df.ob.lost$curated.name <- reorder(df.ob.lost$curated.name, df.ob.lost$NES)

ob.lost_palette=c("#de77ae", "#7fbc41")
Figure_3D_OBAtt<-plot_GSEA_collapsed(df.ob.lost,ob.lost_palette)#+rremove("y.text")+rremove("legend")

##### Figure 3D (POB Specific) ######
# POB Specific GSEA results

df.pob.spec<-create_collapesed_df(df,"POB_spec")
df.pob.spec$curated.name <- reorder(df.pob.spec$curated.name, df.pob.spec$NES)

pob_palette=c("#7fbc41")
Figure_3D_POBSpec<-plot_GSEA_collapsed(df.pob.spec,pob_palette)#+rremove("y.text")+rremove("legend")




