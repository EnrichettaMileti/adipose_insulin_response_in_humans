Figure S5
================
Enrichetta Mileti
03/03/2021

### This script will generate Figure S5

#### Load Libraries

``` r
library(VennDiagram)
library(ggpubr)
library(ggrepel)
library(data.table)
library(dplyr)
library(tidyr)
```

#### Load DE file

``` r
load("/Data/Addtional Data/DE/DE_23NO_23OB_23POB.RData")
```

#### Open Ribosomal TFBS Results file

``` r
rib<-data.frame(fread("/Data/Addtional Data/TFBS/Ribosomal_allEnrichments.tsv",header=T,sep="\t"))
```

#### Add additional columns

Add columns function will add additional columns to each dataframe that
will be needed for Figure 5A, 5B and 5C.

You should specify the df of interest (e.g. common) and the name of the
df

Column Sign that discriminate between Significant and Non-Significant
results

Column log2odds

Column Type

Column CellType: if the pValueLog is \>=2 it will add the cellType
otherwise it will set as none.

This column will be used to plot different symbols for the significant
TFBS based on the celltype

``` r
add_columns<-function(df,group){
  df$sign<-ifelse(df$pValueLog >=2,"sign","non-sign")
  df$log2odds<-log2(df$oddsRatio+1)
  df$type<-group
  df$CellType<-as.factor(ifelse(df$pValueLog >=2,as.character(df$cellType),'none'))
  
  return(df)
}
rib<-add_columns(rib, "Rib")

rib_palette<-c("black")
rib.2<-data.frame(rib %>% group_by(collection) %>% summarise(pValueLog = max(pValueLog)))
rib.2$max<-"max"
rib.3<-merge(rib,rib.2,by=c("collection","pValueLog"),all.x=T)
rib.3<-rib.3 %>%
  mutate_at(vars(max), ~replace_na(., "other"))
rib.3$class<-ifelse(rib.3$sign!="non-sign" & rib.3$max=="max" & rib.3$pValueLog>12,"max",
                    ifelse(rib.3$sign!="non-sign" & rib.3$max=="other","other","non-sign"))
```

## Figure S5

To show the top 10 enriched TFBSs, we set pval cut-off to 22.9

``` r
Figure_S5<-ggscatter(rib.3,x="pValueLog",y="log2odds",color="class"
          ,fill="black",label="collection",palette=c("black","grey","grey")
          ,shape='sign',label.select = list(criteria = "`pValueLog` >= 12 & `class`==max")
          ,repel=TRUE)+border()+geom_vline(xintercept = 2)+rremove("legend")+rremove("xlab")+rremove("ylab")
Figure_S5
```

![](/Other/FigureS5/FigureS5-unnamed-chunk-5-1.png)<!-- -->
