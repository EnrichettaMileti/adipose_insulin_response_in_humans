Figure 1AF
================
Enrichetta Mileti
23/02/2021

### This script will generate Figure 1AF panel

#### Load Libraries

``` r
library(ggpubr)
library(reshape2)
```

#### Load expression table, clinical table and cohort table

``` r
load("/Data/ExpressionData/ExpressionTables.RData")
load("/Data/Cohort/Cohort.RData")
load("clinical.RData")
```

#### Read M-value Table

``` r
Mvalues<-read.table("M_and_M_by_I_2.txt",header=T,sep="\t")
Mvalues.m<-melt(Mvalues)
```

#### M-value

``` r
M<-Mvalues.m[Mvalues.m$Type=="M",]
```

#### Normalized M-value

``` r
norm_M<-Mvalues.m[!Mvalues.m$Type=="M",]
```

#### BMI change over year table

``` r
bmi_change<-read.table("BMI_0_1_2_years.txt",header=T,sep="\t")
bmi_change$subj<-paste("S",rownames(bmi_change),sep="")

bmi_change.1<-melt(bmi_change)
bmi_change.1$group<-ifelse(bmi_change.1$variable=="Calculated_BMI_0","Obese",
                           ifelse(bmi_change.1$variable=="Calculated_BMI_2","Post-Obese_2","Post-Obese_1"))

bmi_change.1$year<-ifelse(bmi_change.1$variable=="Calculated_BMI_0","0",
                          ifelse(bmi_change.1$variable=="Calculated_BMI_2","2","1"))

bmi_change.1$group2<-"Obese"
bmi_change.2 <- data.frame(group=bmi_change.1$group, variable=bmi_change.1$variable, value=bmi_change.1$value, subj=bmi_change.1$subj, U = interaction(bmi_change.1$year, bmi_change.1$group2))
```

#### Read Matched NO-POB samples

``` r
pairs<-read.table("/Users/enrichettamileti/OneDrive - KI.SE/Share Carsten Enrichetta papers/WeightLoss_Sept2020/Data/Pairs_NO_POB.txt",header=F,sep="\t")
samples<-pairs$V1
```


## Figure 1A

BMI Change over
time

``` r
bmi_change.2$Time <- factor(bmi_change.2$U, levels = c("0.Obese", "1.Obese", "2.Obese"))
levels(bmi_change.2$Time) <- list("0"="0.Obese", "1"="1.Obese", "2"="2.Obese")

Figure_1A<-ggboxplot(bmi_change.2,x="Time",y="value",fill="Time",palette=c("#5590CC","white","#7fbc41")
          ,ylab="BMI (kg/m2)")+border()+rremove("xlab")+rremove("legend")
Figure_1A
```

![](/Other/Figure1AF/Figure1AF-unnamed-chunk-9-1.png)<!-- -->

## Figure 1B

Age

``` r
clinical$Type2 <- factor(clinical$Type, levels = c("OBESE", "POST_OBESE", "NO"))
levels(clinical$Type2) <- list(OB="OBESE", POB="POST_OBESE", NO="NO")

Figure_1CB-ggboxplot(clinical, x="Type2",y="Age",fill="Type",palette=c("#de77ae","#5590CC","#7fbc41")
          ,ylab="Age (years)")+border()+rremove("xlab")+rremove("legend")
Figure_1B
```

![](/Other/Figure1AF/Figure1AF-unnamed-chunk-10-1.png)<!-- -->

## Figure 1C

BMI

``` r
Figure_1C<-ggboxplot(clinical, x="Type2",y="BMI",fill="Type",palette=c("#de77ae","#5590CC","#7fbc41")
          ,ylab="BMI (kg/m2)")+border()+rremove("xlab")+rremove("legend")
Figure_1C
```
![](/Other/Figure1AF/Figure1AF-unnamed-chunk-11-1.png)<!-- -->

## Figure 1D

HOMA
``` r
clinical$HOMA_IR<-(clinical$P_glukos*clinical$Ins)/22.5
Figure_1D<-ggboxplot(clinical, x="Type2",y="HOMA_IR",fill="Type",palette=c("#de77ae","#5590CC","#7fbc41")
                     ,ylab="Age (years)")+border()+rremove("xlab")+rremove("legend")

Figure_1D
```
![](/Other/Figure1AF/Figure1AF-unnamed-chunk-14-1.png)<!-- -->

## Figure 1E

M-value
(mg/kg\*min)

``` r
M$Type<-factor(M$variable, levels = c("Pre.RYGB", "Post.RYGB", "Controls"))
levels(M$Type)<- list(OB="Pre.RYGB", POB="Post.RYGB", NO="Controls")

Figure_1E<-ggboxplot(M,x = "Type", y = "value",fill="Type",palette=c("#5590CC","#7fbc41","#de77ae")
          ,ylab="M-value (mg/kg*min)")+border()+rremove("xlab")+rremove("legend")
Figure_1E
```

![](/Other/Figure1AF/Figure1AF-unnamed-chunk-12-1.png)<!-- -->

## Figure 1F

M-value/Insulin (mg/kg\*min\*mean
P-insulin)

``` r
norm_M$Type<-factor(norm_M$variable, levels = c("Pre.RYGB", "Post.RYGB", "Controls"))
levels(norm_M$Type)<- list(OB="Pre.RYGB", POB="Post.RYGB", NO="Controls")

Figure_1F<-ggboxplot(norm_M,x = "Type", y = "value",fill="Type",palette=c("#5590CC","#7fbc41","#de77ae")
          ,ylab="M-value/Insulin (mg/kg*min*mean P-Insulin)")+border()+rremove("xlab")+rremove("legend")
Figure_1F
```

![](/Other/Figure1AF/Figure1AF-unnamed-chunk-13-1.png)<!-- -->
