Figure3 A-B
================
Enrichetta Mileti
03/03/2021

### This script will generate Figure 3 A-B panel

#### Load Libraries

``` r
library(dplyr)
library(data.table)
library(scales)
library(ggpubr)
library(tidyr)
```

#### Reading the results files and add additional information needed for the plots

#### Open Results file

``` r
no<-data.frame(fread("/Data/Additional Data/TFBS/NO_allEnrichments.tsv",header=T,sep="\t"))
ob<-data.frame(fread("/Data/Additional Data/TFBS/OB_allEnrichments.tsv",header=T,sep="\t"))
pob<-data.frame(fread("/Data/Additional Data/TFBS/POB_allEnrichments.tsv",header=T,sep="\t"))
```

#### Add additional columns

add columns function will add additional columns to each dataframe that
will be needed for Figure 5A, 5B and 5C You should specify the df of
interest (e.g. common) and the name of the df Column Sign that
discriminate between Significant and Non-Significant results Column
log2odds Column Type  
Column CellType: if the pValueLog is \>=2 it will add the cellType
otherwise it will set as none. This column will be used to plot
different symbols for the significant TFBS based on the celltype

``` r
add_columns<-function(df,group){
  df$sign<-ifelse(df$pValueLog >=2,"sign","non-sign")
  df$log2odds<-log2(df$oddsRatio+1)
  df$type<-group
  df$CellType<-as.factor(ifelse(df$pValueLog >=2,as.character(df$cellType),'none'))
  
  return(df)
}
no<-add_columns(no, "NO")
ob<-add_columns(ob,"OB")
pob<-add_columns(pob,"POB")
```

#### combine TFBSs results and remove non significant TFBSs

#### To identify groups of TFBSs

``` r
df<-rbind.data.frame(no,ob,pob)
df<-df[df$sign=="sign",]
df<-df %>% arrange(CellType)
df<-data.frame(df %>%
                 group_by(cellType) %>%
                 mutate(CellType_id = cur_group_id()))

df.1<-unique(data.frame(collection=df$collection,type=df$type))
p<-df.1 %>% group_by(type)
p1<-dcast(df.1,collection~type)
```

    ## Warning in dcast(df.1, collection ~ type): The dcast generic in data.table has
    ## been passed a data.frame and will attempt to redirect to the reshape2::dcast;
    ## please note that reshape2 is deprecated, and this redirection is now deprecated
    ## as well. Please do this redirection yourself like reshape2::dcast(df.1). In the
    ## next version, this warning will become an error.

``` r
p1$name<-paste(paste(p1$NO,p1$OB,sep="_"),p1$POB,sep="_")
p1$name2<-ifelse(p1$name=="NA_NA_POB","POB Specific"
                 ,ifelse(p1$name=="NA_OB_NA","OB Specific"
                         ,ifelse(p1$name=="NA_OB_POB","NO lost"
                                 ,ifelse(p1$name=="NO_NA_NA","NO Specific"
                                         ,ifelse(p1$name=="NO_NA_POB","OB lost"
                                                 ,ifelse(p1$name=="NO_OB_NA","POB lost","Common"))))))


p2<-merge(df.1,p1[,c("collection","name2")],by="collection")
df_bar<-data.frame(p2%>%group_by(name2,type)%>%tally())
df_bar$name2 <- factor(df_bar$name2, levels=c("POB Specific","OB Specific","NO lost","NO Specific","OB lost","POB lost","Common"))
```

## Figure 3A

``` r
Figure_3A<-ggbarplot(df_bar, x = "type", y = "n",fill="name2",label=T,ylab="Number of enriched TFBSs",
               palette =c("#7fbc41", "#CC0000","#4C4CFF","#dd3497","#FFD64D",'white',"#FF8C00")
)+border()+rremove('xlab')+rremove("legend")+rremove("ylab")
Figure_3A
```

![](/Other/Figure3AB/Figure3AB-unnamed-chunk-5-1.png)<!-- -->

#### Formatting Enriched TFBSs tables for plotting

#### NO Enriched TFBSs

``` r
no.1<-merge(no,p2,by=c("collection","type"),all.x=T)
no.1<-unique(merge(no.1,df[,c("cellType","CellType_id")],by="cellType"))
no.1$class<-ifelse(no.1$sign=="non-sign","non-sign"
                   ,ifelse(no.1$sign=="sign" & !is.na(no.1$name2),as.character(no.1$name2),"non-sign"))

no.2<-data.frame(no.1 %>% group_by(collection) %>% summarise(pValueLog = max(pValueLog)))
no.2$max<-"max"
no.3<-merge(no.1,no.2,by=c("collection","pValueLog"),all.x=T)
no.3<-no.3 %>%
  mutate_at(vars(max), ~replace_na(., "other"))
no.3$class2<-ifelse(no.3$class!="non-sign" & no.3$max=="max",as.character(no.3$class),
                    ifelse(no.3$class!="non-sign" & no.3$max=="other","non-sign","non-sign"))
```

#### OB Enriched TFBS

``` r
ob.1<-merge(ob,p2,by=c("collection","type"),all.x=T)
ob.1<-unique(merge(ob.1,df[,c("cellType","CellType_id")],by="cellType"))
ob.1$class<-ifelse(ob.1$sign=="non-sign","non-sign"
                   ,ifelse(ob.1$sign=="sign" & !is.na(ob.1$name2),as.character(ob.1$name2),"non-sign"))
ob.2<-data.frame(ob.1 %>% group_by(collection) %>% summarise(pValueLog = max(pValueLog)))
ob.2$max<-"max"
ob.3<-merge(ob.1,ob.2,by=c("collection","pValueLog"),all.x=T)
ob.3<-ob.3 %>%
  mutate_at(vars(max), ~replace_na(., "other"))
ob.3$class2<-ifelse(ob.3$class!="non-sign" & ob.3$max=="max",as.character(ob.3$class),
                    ifelse(ob.3$class!="non-sign" & ob.3$max=="other","non-sign","non-sign"))
```

#### POB Enriched TFBSs

``` r
pob.1<-merge(pob,p2,by=c("collection","type"),all.x=T)
pob.1<-unique(merge(pob.1,df[,c("cellType","CellType_id")],by="cellType"))
pob.1$class<-ifelse(pob.1$sign=="non-sign","non-sign"
                    ,ifelse(pob.1$sign=="sign" & !is.na(pob.1$name2),as.character(pob.1$name2),"non-sign"))
pob.2<-data.frame(pob.1 %>% group_by(collection) %>% summarise(pValueLog = max(pValueLog)))
pob.2$max<-"max"
pob.3<-merge(pob.1,pob.2,by=c("collection","pValueLog"),all.x=T)
pob.3<-pob.3 %>%
  mutate_at(vars(max), ~replace_na(., "other"))
pob.3$class2<-ifelse(pob.3$class!="non-sign" & pob.3$max=="max",as.character(pob.3$class),
                     ifelse(pob.3$class!="non-sign" & pob.3$max=="other","non-sign","non-sign"))
```

#### TFBSs plots

``` r
TFBS_plots<-function(df,palette){
  ggscatter(df,x="pValueLog",y="log2odds",group="type",color="class2",fill="black",palette=palette,label="collection",shape='sign'
            ,label.select = list(criteria = "`pValueLog` > 2"),repel=TRUE,xlim=c(0,8),ylim=c(0,6)
            ,xlab="-log10(p-value)",ylab="log2(odds ratio+1)",font.label=c('bold')
  )+border()+rremove("legend")+rremove("xlab")+rremove("ylab")
}
```

## Figure 3B (OB)

OB Enriched TFBSs

``` r
ob_palette<-c("#FF8C00", "#4C4CFF","grey","#CC0000",'black')
Figure_3B_OB<-TFBS_plots(ob.3,ob_palette)+geom_vline(xintercept = 2)
```

    ## Warning: `filter_()` is deprecated as of dplyr 0.7.0.
    ## Please use `filter()` instead.
    ## See vignette('programming') for more help
    ## This warning is displayed once every 8 hours.
    ## Call `lifecycle::last_warnings()` to see where this warning was generated.

``` r
Figure_3B_OB
```

![](/Other/Figure3AB/Figure3AB-unnamed-chunk-10-1.png)<!-- -->

## Figure 3B (POB)

POB Enriched TFBSs

``` r
pob_palette<-c("#FF8C00", "#4C4CFF","grey","#FFD64D",'#7fbc41')
Figure_3B_POB<-TFBS_plots(pob.3,pob_palette)+geom_vline(xintercept = 2)
Figure_3B_POB
```

![](/Other/Figure3AB/Figure3AB-unnamed-chunk-11-1.png)<!-- -->

## Figure 3B (NO)

NO Enriched TFBSs

``` r
no_palette<-c("#FF8C00", "#dd3497","grey","#FFD64D",'black')
Figure_3B_NO<-TFBS_plots(no.3,no_palette)+geom_vline(xintercept = 2)
Figure_3B_NO
```

![](/Other/Figure3AB/Figure3AB-unnamed-chunk-12-1.png)<!-- -->
