Figure S3
================
Enrichetta Mileti
03/03/2021

### This script will generate Figure S3 panel

#### Load Libraries

``` r
library(dplyr)
library(data.table)
library(scales)
library(ggpubr)
```

#### Read TFBS categories files

``` r
tfbs_cat<-read.table("/Data/Addtional Data/TFBS/tfbs_categories.txt",header=T,sep="\t")
tfbs_cat.1<-unique(data.frame(collection=tfbs_cat$collection,group_collection=tfbs_cat$group_collection,Set=tfbs_cat$Set))
tfbs_cat.1$Contrast<-ifelse(tfbs_cat.1$Set=="NO","HivsF.NO"
                            ,ifelse(tfbs_cat.1$Set=="OB","HivsF.OB.0","HivsF.OB.2"))
```

### Load expression table, annotation and DE files

``` r
load("/Data/Addtional Data/DE/DE_23NO_23OB_23POB.RData")
load("/Data/Annotation/Annotation.RData")
load("/Data/ExpressionData/ExpressionTables.RData")
glm<-tab
rm(tab)
```

#### DE Table results

``` r
DE.tab<-function(tabs){
  DE<-merge(tabs,annotation,by="TC")
  return(DE)
}

DE.tab.specific<-function(tabs,contrast){
  out<-tabs[tabs$Contrast==contrast,]
  out$Sign<-ifelse(out$FDR<0.05,"sign","no")
  out<-merge(out,annotation,by="TC")
  out$SignUpDown<-ifelse((out$Sign=="sign" & out$updown=="up"),"up",ifelse((out$Sign=="sign" & out$updown=="down"),"down","non-significant"))
  out$log10FDR<-(-log10(out$FDR))
  return(out)
}
```

#### Overall DE

``` r
DE.glm<-DE.tab(glm)
```

#### NO DE tables

``` r
NO.glm<-DE.tab.specific(glm,"HivsF.NO")
```

#### OBESE DE tables

``` r
OB.glm<-DE.tab.specific(glm,"HivsF.OB.0")
```

#### POST-OBESE DE tables

``` r
POB.glm<-DE.tab.specific(glm,"HivsF.OB.2")
```

#### Merge TFBS results with DE results

``` r
tfbs_cat.3<-merge(tfbs_cat.1,DE.glm[,c("TC","logFC","FDR","Contrast","gene")],by.x=c("collection","Contrast"),by.y=c("gene","Contrast"))
```

### TFBS Baloon plot

``` r
TF_baloon_plot<-function(df){
  ggballoonplot(df, x = "collection", y = "TC", size = 4,
                fill = "logFC", facet.by = "Contrast",
                ggtheme = theme_bw())+ rotate_x_text() +
    scale_fill_gradient2(low="blue",mid="white" ,high="red", limits=c(-1, 1), oob=squish)+scale_y_discrete(limits = rev(unique(sort(df$name))))

}
```

## Figure S3A

Common TFBS and their TCs

``` r
common_tfs<-tfbs_cat.3[tfbs_cat.3$group_collection=="common",]
common_tfs$TC<-factor(common_tfs$TC, levels = unique(common_tfs$TC[rev(order(common_tfs$collection))]))

Figure_S2A<-TF_baloon_plot(common_tfs)
Figure_S2A+rremove("legend")
```

![](/Other/FigureS3/FigureS3-unnamed-chunk-11-1.png)<!-- -->

## Figure S3B

OB attenuated TFBS and their TCs

``` r
OBLost_tfs<-tfbs_cat.3[tfbs_cat.3$group_collection=="OB_lost",]
OBLost_tfs$TC<-factor(OBLost_tfs$TC, levels = unique(OBLost_tfs$TC[rev(order(OBLost_tfs$collection))]))

Figure_S2B<-TF_baloon_plot(OBLost_tfs)
Figure_S2B+rremove("legend")
```

![](/Other/FigureS3/FigureS3-unnamed-chunk-12-1.png)<!-- -->

## Figure S3C

POB Specific TFBS and their TCs

``` r
POBspec_tfs<-tfbs_cat.3[tfbs_cat.3$group_collection=="POB_specific",]
POBspec_tfs$TC<-factor(POBspec_tfs$TC, levels = unique(POBspec_tfs$TC[rev(order(POBspec_tfs$collection))]))

Figure_S3C<-TF_baloon_plot(POBspec_tfs[POBspec_tfs$Contrast=="HivsF.OB.2",])
Figure_S3C+rremove("legend")
```

![](/Other/FigureS3/FigureS3-unnamed-chunk-13-1.png)<!-- -->
