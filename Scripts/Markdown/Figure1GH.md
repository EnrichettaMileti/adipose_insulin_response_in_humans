Figure1 G-H
================
Enrichetta Mileti
03/03/2021

### This script will generate Figure 1 G-H panel

#### Load Libraries

``` r
library(ggpubr)
library(ggplot2)
library(dplyr)
library(tidyr)
library(scales)
library(reshape2)
library(tidyverse)
```

#### Load expression table and cohort table

``` r
load("/Data/ExpressionData/ExpressionTables.RData")
load("/Data/Additional Data/DE/DE_23NO_23OB_23POB.RData")
load("/Data/Cohort/Cohort.RData")
```

#### Load TC annotation

``` r
load("/Data/Annotation/Annotation.RData")
```

#### DE table

Extract only DE tc.

It also create additional 3 dataframes, one per condition (NO,OBESE and
POST-OBESE)

``` r
glm<-tab

DE.tab<-function(tabs){
  DE<-tabs[tabs$FDR<0.05,]
  DE<-merge(DE,annotation,by="TC")
  
  return(DE)
}

DE.tab.specific<-function(tabs,contrast){
  out<-tabs[tabs$Contrast==contrast,]
  out$Sign<-ifelse(out$FDR<0.05,"sign","no")
  out<-merge(out,annotation,by="TC")
  out$SignUpDown<-ifelse((out$Sign=="sign" & out$updown=="up"),"up",ifelse((out$Sign=="sign" & out$updown=="down"),"down","non-significant"))
  out$log10FDR<-(-log10(out$FDR))
  return(out)
}
```

#### Overall DE

``` r
DE.glm<-DE.tab(glm)
```

#### NO DE tables

``` r
NO.glm<-DE.tab.specific(glm,"HivsF.NO")
```

#### OBESE DE tables

``` r
OB.glm<-DE.tab.specific(glm,"HivsF.OB.0")
```

#### POST-OBESE DE tables

``` r
POB.glm<-DE.tab.specific(glm,"HivsF.OB.2")
```

## Figure 1G: Barplot of DE TCs

``` r
bars<-function(tab){
  tab$Sign<-ifelse(tab$FDR<0.05,"sign","no")
  res.count <-tab[tab$Sign=="sign",]%>%group_by(Contrast,updown)%>%tally()
  res.count <- (data.frame(res.count))
  res.count$col<-paste(res.count$Contrast,res.count$updown,sep=".")
  res.count$n2<-ifelse(res.count$updown=="up",as.numeric(res.count$n),as.numeric(-res.count$n))
  res.count$Contrast<-factor(res.count$Contrast, levels = c("HivsF.OB.0", "HivsF.OB.2", "HivsF.NO"))
  levels(res.count$Contrast)<- list("OB"="HivsF.OB.0", "POB"="HivsF.OB.2", "NO"="HivsF.NO")
  bar<-ggbarplot(res.count, x = "Contrast", y = "n2", fill = "col",rotate = F,ylab = "Number of differential expressed TC",
                 
                 palette = c("#f1b6da","#de77ae","#9FC9EB", "#5590CC","#b8e186","#7fbc41"),label=T,lab.pos = c("in"))+ 
    geom_hline(yintercept=0)+border()+ylim(-50,500)+rremove('legend')
  #bar+ scale_x_discrete(labels=c("NO (hi/f)", "OB (hi/f)", "POB (hi/f)"))+rremove("xlab")
}
Figure1G<-bars(glm)
Figure1G
```

![](/Other/Figure1GH/Figure1GH-unnamed-chunk-10-1.png)<!-- -->

#### Number of unique differentially expressed genes

``` r
bars.gene<-function(tab){
  tab$Sign<-ifelse(tab$FDR<0.05,"sign","no")
  tab<-tab[!(tab$gene)=="NA",]
  res.count <-tab[tab$Sign=="sign",]%>%group_by(Contrast,updown)%>%summarise(n=n_distinct(gene))
  
  res.count <- (data.frame(res.count))
  res.count$col<-paste(res.count$Contrast,res.count$updown,sep=".")
  res.count$n2<-ifelse(res.count$updown=="up",as.numeric(res.count$n),as.numeric(-res.count$n))
  res.count$Contrast<-factor(res.count$Contrast, levels = c("HivsF.OB.0", "HivsF.OB.2", "HivsF.NO"))
  res.count
}
Figure_1G_genes<-bars.gene(DE.glm)
Figure_1G_genes
```

    ##     Contrast updown   n             col  n2
    ## 1   HivsF.NO   down  15   HivsF.NO.down -15
    ## 2   HivsF.NO     up 148     HivsF.NO.up 148
    ## 3 HivsF.OB.0   down   6 HivsF.OB.0.down  -6
    ## 4 HivsF.OB.0     up  64   HivsF.OB.0.up  64
    ## 5 HivsF.OB.2   down  22 HivsF.OB.2.down -22
    ## 6 HivsF.OB.2     up 220   HivsF.OB.2.up 220

## Figure 1H: PCA

Perform PCA for DE TCs

``` r
pca<-function(DE,data){
  tc<-unique(DE[DE$FDR<0.05,]$TC)
  print(length(tc))
  dat.1<-data[rownames(data) %in% tc,]
  dat.1<-dat.1[,colnames(dat.1) %in% cohort$Cond]
  mydata.pca <- prcomp(t(dat.1),center=T,scale=T)
  return(mydata.pca)
}

pca_summary<-function(pca_data){
  summary<-summary(pca_data)
  
  return(summary)
}

df_pca<-function(mydata.pca,cohort){
  scores=as.data.frame(mydata.pca$x)[1:3]
  scores$Sample<-rownames(scores)
  df<-merge(cohort, scores, by.x="Cond", by.y="Sample",sort=T)
  df$newcond2<-factor(df$newcond2, levels = c("NO.f0","NO.h0","OBESE.f0","OBESE.h0","OBESE.f2","OBESE.h2"))
  return(df)
}

pca_plot2<-function(df,summary,title){

  xmin<-min(df$PC1)
  xmax<-max(df$PC1)
  ymin<-min(df$PC2)
  ymax<-max(df$PC2)
  df_mean<-aggregate(df[, 27:29], list(df$newcond3), mean)
  
  df_mean$Type<-ifelse((df_mean$Group.1=="NO.f"|df_mean$Group.1=="NO.h"),"NO"
                       ,ifelse((df_mean$Group.1=="OB.f"|df_mean$Group.1=="OB.h"),"OB","POB"))
  
 
  p<-ggplot(df, aes(x=PC1, y=PC2)) 
  
  p+ geom_point(aes(shape=OB_NO_POB, fill=newcond3,color=newcond3,stroke = 0.7),size=4)+
    scale_shape_manual(values=c(21,22,23))+
    scale_fill_manual(values=alpha(c("#f1b6da","#de77ae","#9FC9EB", "#5590CC","#b8e186","#7fbc41"),0.7)
    )+
    scale_color_manual(values=c("#f1b6da","#de77ae","#9FC9EB", "#5590CC","#b8e186","#7fbc41")
                       ,labels=c("NO.f", "NO.h", "OB.f", "OB.h","POB.f", "POB.h"))+
    geom_point(data=df_mean, aes(PC1, PC2,shape=Type,fill=Group.1),
               size=6)+geom_line(data=df_mean,aes(group = Type),arrow=arrow(angle=10,type = "closed"))+theme_bw() +
    theme(panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          panel.border = element_rect(colour = "black", fill=NA, size=.7),
          axis.text.x = element_text(size=10))+
    labs(x=paste("PC1=",percent(summary$importance[2,1]),sep=" "),y=paste("PC2=",percent(summary$importance[2,2]),sep=" "))+rremove("legend")
  
}

pca_x_marginal<-function(df,summary,title){
  xplot2 <- ggboxplot(df,  x = "newcond3", y = "PC1", 
                     color = "newcond3", fill = "newcond3", palette = c( "#5590CC","#de77ae","#7fbc41","#9FC9EB","#f1b6da","#b8e186")
                     ,order = c( "OB.h","NO.h",  "POB.h","OB.f","NO.f","POB.f")
                     ,alpha = 0.5,rotate=T)+ border()+rremove("legend")+rremove("ylab")
  xplot2
}

pca_y_marginal<-function(df,summary,title){
  
  yplot2 <- ggboxplot(df,  x = "newcond3", y = "PC2", 
                     color = "newcond3", fill = "newcond3", palette = c( "#9FC9EB","#f1b6da","#b8e186","#5590CC","#de77ae","#7fbc41")
                     ,order = c( "OB.f","NO.f","POB.f","OB.h","NO.h",  "POB.h")
                     ,alpha = 0.5,rotate=F,
                     x.text.angle=90)+ border()+rremove("legend")+rremove("ylab")
  yplot2
}

pca_glm<-(pca(DE.glm,phase2.female.tpm))
```

    ## [1] 427

``` r
Figure1H<-pca_plot2(df_pca(pca_glm),pca_summary(pca_glm),"S94")
Figure1H
```

![](/Other/Figure1GH/Figure1GH-unnamed-chunk-12-1.png)<!-- -->

``` r
Figure1H.x2<-pca_x_marginal(df_pca(pca_glm))
Figure1H.x2+rremove("xlab")+rremove("ylab")+coord_flip()
```

![](/Other/Figure1GH/Figure1GH-unnamed-chunk-12-2.png)<!-- -->

``` r
Figure1H.y2<-pca_y_marginal(df_pca(pca_glm))
Figure1H.y2+rremove("xlab")+rremove("ylab")#+ylim(-20,25)+rremove("xy.text")
```

![](/Other/Figure1GH/Figure1GH-unnamed-chunk-12-3.png)<!-- -->
