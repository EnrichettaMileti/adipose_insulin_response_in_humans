Figure S2
================
Enrichetta Mileti
04/03/2021

### This script will generate Figure S2

#### Load Libraries

``` r
library(ggpubr)
library(ggplot2)
library(dplyr)
library(tidyr)
library(scales)
library(reshape2)
library(tidyverse)
```

#### Load Expression and DE data

``` r
load("/Data/ExpressionData/ExpressionTables.RData")
load("/Data/Additional Data/DE/DE_23NO_23OB_23POB.RData")
load("/Data/Cohort/Cohort.RData")
```

#### TC Annotation

``` r
load("/Data/Annotation/Annotation.RData")
```

##### DE tables

Extract only DE tc. It also create additional 3 dataframes, one per
condition(NO,OBESE and POST-OBESE)

``` r
glm<-tab

DE.tab<-function(tabs){
  DE<-tabs[tabs$FDR<0.05,]
  DE<-merge(DE,annotation,by="TC")

  return(DE)
}

DE.tab.specific<-function(tabs,contrast){
  out<-tabs[tabs$Contrast==contrast,]
  out$Sign<-ifelse(out$FDR<0.05,"sign","no")
  out<-merge(out,annotation,by="TC")
  out$SignUpDown<-ifelse((out$Sign=="sign" & out$updown=="up"),"up",ifelse((out$Sign=="sign" & out$updown=="down"),"down","non-significant"))
  out$log10FDR<-(-log10(out$FDR))
  return(out)
}
```

#### Overall DE

``` r
DE.glm<-DE.tab(glm)
```

#### NO DE tables

``` r
NO.glm<-DE.tab.specific(glm,"HivsF.NO")
```

#### OBESE DE tables

``` r
OB.glm<-DE.tab.specific(glm,"HivsF.OB.0")
```

#### POST-OBESE DE tables

``` r
POB.glm<-DE.tab.specific(glm,"HivsF.OB.2")
```

#### Add Common\_Specific to the overall DE table

``` r
NO_POB<-(intersect(NO.glm[NO.glm$FDR<0.05,]$TC,POB.glm[POB.glm$FDR<0.05,]$TC))
NO_OB<-(intersect(NO.glm[NO.glm$FDR<0.05,]$TC,OB.glm[OB.glm$FDR<0.05,]$TC))
OB_POB<-(intersect(OB.glm[OB.glm$FDR<0.05,]$TC,POB.glm[POB.glm$FDR<0.05,]$TC))
NO_OB_POB<-intersect(intersect(NO.glm[NO.glm$FDR<0.05,]$TC,OB.glm[OB.glm$FDR<0.05,]$TC),POB.glm[POB.glm$FDR<0.05,]$TC)

NO_POB_2<-NO_POB[!NO_POB %in% NO_OB_POB]
NO_OB_2<-NO_OB[!NO_OB %in% NO_OB_POB]
OB_POB_2<-OB_POB[!OB_POB %in% NO_OB_POB]


DE.glm$Comm_Spec<-ifelse(DE.glm$TC %in% NO_POB & (!(DE.glm$TC %in% NO_OB_POB)) ,"NO_POB"  
                         ,ifelse(DE.glm$TC %in% NO_OB & (!(DE.glm$TC %in% NO_OB_POB)),"NO_OB"
                                 ,ifelse(DE.glm$TC %in% OB_POB & (!(DE.glm$TC %in% NO_OB_POB)),"OB_POB"
                                         ,ifelse(DE.glm$TC %in% NO_OB_POB,"NO_OB_POB",DE.glm$Contrast))))
NO_spec<-DE.glm[DE.glm$Comm_Spec=="HivsF.NO",]$TC
OB_spec<-DE.glm[DE.glm$Comm_Spec=="HivsF.OB.0",]$TC
POB_spec<-DE.glm[DE.glm$Comm_Spec=="HivsF.OB.2",]$TC

NO.glm.sign<-unique(merge(NO.glm[NO.glm$FDR<0.05,],DE.glm[, c("TC", "Comm_Spec")], by = "TC"))
OB.glm.sign<-unique(merge(OB.glm[OB.glm$FDR<0.05,],DE.glm[, c("TC", "Comm_Spec")], by = "TC"))
POB.glm.sign<-unique(merge(POB.glm[POB.glm$FDR<0.05,],DE.glm[, c("TC", "Comm_Spec")], by = "TC"))

NO.glm.sign.2<-data.frame(unique(merge(NO.glm,DE.glm[, c("TC", "Comm_Spec")], by = "TC",all.x=T)))
OB.glm.sign.2<-unique(merge(OB.glm,DE.glm[, c("TC", "Comm_Spec")], by = "TC",all.x=T))
POB.glm.sign.2<-unique(merge(POB.glm,DE.glm[, c("TC", "Comm_Spec")], by = "TC",all.x=T))
```


#### Set color palette and shapes

``` r
palette_logfc_NO_POB<-c("#FF8C00","#FFD64D")
shape_NO_POB<-c(21,22)
palette_logfc_NO_OB<-c("#FF8C00")
shape_NO_OB<-c(21)
palette_logfc_OB_POB<-c("#FF8C00","#4C4CFF")
shape_OB_POB<-c(21,25)
```

#### fc plot function

``` r
fc_plot<-function(tab1,tab2,var1,var2,palette,shapes,remove){
  df<-merge(tab1,tab2,by.x="TC",by.y="TC",all=T)
  df<-df[complete.cases(df), ]
  df<-df[df$Comm_Spec.x!=remove,]
  ggscatter(df,x="logFC.x",y="logFC.y",color="Comm_Spec.x",shape="Comm_Spec.x",colour="black",ylab=paste("logFC(hi/f)",var2,sep=" "),xlab=paste("logFC(hi/f)",var1,sep=" "),legend="top",alpha=0.7,size=4
            ,fill="Comm_Spec.x",palette=palette)+xlim(-3,7.5)+ geom_abline(intercept = 0, slope = 1)+ylim(-3,7.5)+border()+
    scale_shape_manual(values=shapes)#+rremove('legend')

}
```

## Figure S2A

``` r
Figure_S2A<-fc_plot(NO.glm.sign,POB.glm.sign,"Non-obese","Post-Obese",palette_logfc_NO_POB,shape_NO_POB,"")
Figure_S2A
```

![](/Other/FigureS2/FigureS2-unnamed-chunk-13-1.png)<!-- -->

## Figure S2B

``` r
Figure_S2B<-fc_plot(OB.glm.sign,POB.glm.sign,"Obese","Post-Obese",palette_logfc_OB_POB,shape_OB_POB,"OB_POB")
Figure_S2B
```

![](/Other/FigureS2/FigureS2-unnamed-chunk-14-1.png)<!-- -->

## Figure S2C

``` r
Figure_S2C<-fc_plot(OB.glm.sign,NO.glm.sign,"Obese","Non-obese",palette_logfc_NO_OB,shape_NO_OB,"NO_OB")
Figure_S2C
```

![](/Other/FigureS2/FigureS2-unnamed-chunk-15-1.png)<!-- -->

## Figures S2D,S2E, S2F: logFC POB Specific TC plots

Plot FC only for POB Specific TCs

``` r
fc_plot_onlygreen<-function(tab1,tab2,var1,var2){
  df<-merge(tab1,tab2,by.x="TC",by.y="TC",all=T)
  df<-df[df$Sign.x=="no" & df$Sign.y=="sign" & df$Comm_Spec.y=="HivsF.OB.2",]
  ggscatter(df,x="logFC.x",y="logFC.y",color="Comm_Spec.y",shape=23,colour="black",ylab=paste("logFC(hi/f)",var2,sep=" "),xlab=paste("logFC(hi/f)",var1,sep=" "),legend="top",alpha=0.7,size=4
            ,fill="Comm_Spec.y",palette="#7fbc41")+xlim(-3,7.5)+ geom_abline(intercept = 0, slope = 1)+ylim(-3,7.5)+border()+rremove('legend')

}

fc_plot_onlygreen_NO_OB<-function(tab1,tab2,tab3,var1,var2){
  df<-merge(tab1,tab2,by.x="TC",by.y="TC",all=T)
  pob<-tab3[tab3$Comm_Spec=="HivsF.OB.2" & tab3$Sign=="sign",]
  df<-df[df$Sign.x=="no" & df$Sign.y=="no" & (df$TC %in% pob$TC),]
  ggscatter(df,x="logFC.x",y="logFC.y",color="Comm_Spec.y",shape=23,colour="black",ylab=paste("logFC(hi/f)",var2,sep=" "),xlab=paste("logFC(hi/f)",var1,sep=" "),legend="top",alpha=0.7,size=4
            ,fill="Comm_Spec.y",palette="#7fbc41")+xlim(-3,7.5)+ geom_abline(intercept = 0, slope = 1)+ylim(-3,7.5)+border()+rremove('legend')

}
```

## Figure S2D

``` r
Figure_S2D<-fc_plot_onlygreen(NO.glm.sign.2,POB.glm.sign.2,"Non-obese","Post-Obese")
Figure_S2D
```

![](/Other/FigureS2/FigureS2-unnamed-chunk-17-1.png)<!-- -->

## Figure S2E

``` r
Figure_S2E<-fc_plot_onlygreen(OB.glm.sign.2,POB.glm.sign.2,"Obese","Post-Obese")

Figure_S2E
```

![](/Other/FigureS2/FigureS2-unnamed-chunk-18-1.png)<!-- -->

## Figure S2F

``` r
Figure_S2F<-fc_plot_onlygreen_NO_OB(OB.glm.sign.2,NO.glm.sign.2,POB.glm.sign.2,"Obese","Non-obese")
Figure_S2F
```

![](/Other/FigureS2/FigureS2-unnamed-chunk-19-1.png)<!-- -->
